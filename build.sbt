import Syntax._

name := "geoportal-scanner"

version := "0.1"

scalaVersion := "2.13.4"

idePackagePrefix.invisible := Some("dev.mateuszjaje.geoportalscanner")

val zioVersion = "1.0.5"
libraryDependencies ++= Seq(
  "org.jsoup"                      % "jsoup"                    % "1.+",
  "org.apache.pdfbox"              % "pdfbox"                   % "2.0.22",
  "org.apache.pdfbox"              % "pdfbox-tools"             % "2.0.22",
  "dev.zio"                       %% "zio"                      % zioVersion,
  "dev.zio"                       %% "zio-macros"               % zioVersion,
  "org.typelevel"                 %% "cats-core"                % "2.3.0",
  "com.softwaremill.sttp.client3" %% "core"                     % "3.0.0",
  "com.softwaremill.sttp.client3" %% "httpclient-backend-zio"   % "3.0.0",
  "com.github.pureconfig"         %% "pureconfig"               % "0.14.0",
  "com.github.pathikrit"          %% "better-files"             % "3.9.1",
  "com.github.scopt"              %% "scopt"                    % "4.+",
  "org.mongodb.scala"             %% "mongo-scala-driver"       % "4.2.2",
  "ch.qos.logback"                 % "logback-classic"          % "1.3.0-alpha5",
  "org.slf4j"                      % "slf4j-api"                % "2.0.0-alpha1",
  "org.scalatest"                 %% "scalatest-flatspec"       % "3.2.3" % Test,
  "org.scalatest"                 %% "scalatest-shouldmatchers" % "3.2.3" % Test,
  "org.seleniumhq.selenium"        % "selenium-firefox-driver"  % "3.+",
  "org.seleniumhq.selenium"        % "selenium-java"            % "3.+",
  "org.seleniumhq.selenium"        % "htmlunit-driver"          % "2.47.1",
  "dev.doamaral"                  %% "zinteract"                % "0.2.0",
)

scalacOptions += "-Ymacro-annotations"
